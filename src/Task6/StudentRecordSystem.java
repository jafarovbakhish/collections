package Task6;

import java.util.HashMap;

class StudentRecordSystem {
    private HashMap<Integer, Student> studentMap;

    public StudentRecordSystem() {
        studentMap = new HashMap<>();
    }

    public void addStudent(Student student) {
        studentMap.put(student.getId(), student);
    }

    public void removeStudent(int id) {
        studentMap.remove(id);
    }

    public Student getStudentById(int id) {
        return studentMap.get(id);
    }

    public void printAllStudents() {
        for (Student student : studentMap.values()) {
            System.out.println(student);
        }
    }
}