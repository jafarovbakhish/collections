package Task6;

public class Main {
    public static void main(String[] args) {
        StudentRecordSystem recordSystem = new StudentRecordSystem();

        Student student1 = new Student(999, "Bakhish", 5.0);
        Student student2 = new Student(333, "Eldjan", 5.0);

        recordSystem.addStudent(student1);
        recordSystem.addStudent(student2);

        recordSystem.printAllStudents();

        System.out.println("Student with ID 1: " + recordSystem.getStudentById(1));

        recordSystem.removeStudent(2);

        System.out.println("After removing student with ID 2:");
        recordSystem.printAllStudents();
    }
}